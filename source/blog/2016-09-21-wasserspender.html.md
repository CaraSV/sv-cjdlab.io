---
title: "Schülerkommentar: Wasserspender"
summary: Die neuen Wasserspender. Wie kommt das eigentlich?
author: Ture Raffel, 11b und Ben Bals, 11b (Antwort der SchülerVertretung)
---

*Der folgende Text ist ein Schülerkommentar von True Raffel über die neuen Wasserspender.*

Mein Ranzen ist sehr schwer. Wenn ich frühmorgens das Haus verlasse,
drückt mich das Gewicht meiner Hefter und Bücher regelrecht zu Boden.
Umso mehr freue ich mich natürlich, dass ich nun eine Möglichkeit habe,
etwas Gewicht beim aus dem Haus gehen zu sparen.

Anstatt nun also morgens meine Flasche zu Hause mit ordinärem Leitungswasser zu fülllen,
fülle ich sie an einem der beiden neuen Wasserspender der Schule mit
wahlweise eisgekühltem ?, Wasser mit Sprudel oder stillem Quellwasser, das
sich aber geringerer Beliebtheit erfeurt als die beiden anderen Alternativen.

Schade ist nur, dass zugunsten der Wasserspender der Kaffeeautomat in Haus 2
abgeschaltet werden musste. Vielleicht findet die Schule ja einen Weg das Problem
zu beheben, besonders da der Winter wieder vor der Tür steht.

-----

## Antwort der SchülerVertung

Lieber Ture,

wir freuen uns, dass wir gemeinsam mit der Schulleitung etwas tun konnten um deinen Schulalltag und den deiner Mitschüler zu verbessern.
Wir werden uns auch weiter darum bemühen.

Deine Kritik bezüglich des Kaffeeautomaten haben wir gehört und werden uns dafür einsetzen,
dass du pünktlich zur kalten Jahreszeit wieder deinen Kaffee genießen kannst.

Mit feundlichen Grüßen

Deine SchülerVertretung
